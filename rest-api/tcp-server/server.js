import Net from 'net'
import express from 'express'
const app = express();
const sharedWidgets  = {};
let clientId = 1;
import chalk from 'chalk'        
let consoleLogs = [];
app.use(express.static('../public'));

function logToConsole(message, type = 'log', jsonData = null) {
    const logEntry = { 
        message, 
        type, 
        timestamp: new Date().toISOString(),
        jsonData // Speichern der zusätzlichen Daten
    };
    consoleLogs.push(logEntry);
    // Direkte Verwendung von console.log und console.error
    if (type === 'error') {
        console.error(chalk.red(message));
    } else {
        console.log(chalk.green(message));
    }
}


export function main() {
    console.log(chalk.green('Konsole läuft über: http://localhost:3000/logs.html'))
    startTcpServer();
    startRestApi();
    logToConsole('Server gestartet', 'log');

}

function startTcpServer() {
    const server = new Net.Server();
    const port = 2002;

    server.listen(port, function() {
        logToConsole(`TCP Server listening on localhost:${port}`,);
    });

    server.on('connection', function(socket) {
        const currentClientId = clientId++;
        logToConsole(`Neue Connection ;> Client ID: ${currentClientId}`);

        let buffer = '';

        socket.on('data', function(chunk) {
            buffer += chunk.toString();
            let delimiterIndex = buffer.indexOf('\0');
            while (delimiterIndex !== -1) {
                const message = buffer.substring(0, delimiterIndex);
                buffer = buffer.substring(delimiterIndex + 1);

                if (message.length > 0) {
                    try {
                        const jsonData = JSON.parse(message);
                        handleReceivedJson(jsonData, currentClientId);
                    } catch (error) {
                        logToConsole('Fehler Json:', 'error');
                    }
                }

                delimiterIndex = buffer.indexOf('\0');
            }
        });

        socket.write('Hello, client.\0');
        socket.on('end', () => logToConsole(`Connection beendet mit Client ID :< ${currentClientId}`));
        socket.on('error', (err) => logToConsole(`Fehler: ${err}`, 'error'));
    });
}

function startRestApi() {
    const port = 3000;

    app.get('/api/widgets', (req, res) => {
        res.json(sharedWidgets);
    });

    app.listen(port, () => {
        logToConsole(`REST API listening on port ${port}`);
    });

    app.get('/api/konsole', (req, res) => {
        res.json(consoleLogs);
    });
}
function handleReceivedJson(jsonData, clientId) {
    if (jsonData.kind === 'keepalive') {
        // Kein Log für Keepalive-Nachrichten
        return;
    }

    let logMessage = `Daten von Client ${clientId} erhalten.`;
    if (jsonData.title) {
        if (sharedWidgets[jsonData.title]) {
            logMessage = `Daten für Widget ${jsonData.title} von Client ${clientId} aktualisiert.`;
        }
        sharedWidgets[jsonData.title] = jsonData;
    }

    logToConsole(logMessage, 'log', jsonData); // jsonData als dritter Parameter
}




