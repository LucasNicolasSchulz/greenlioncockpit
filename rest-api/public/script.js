function createWidget(widgetData) {
  const widgetDiv = document.createElement('div');
  const title = widgetData.title.replace(/\s+/g, '-');
  widgetDiv.className = 'widget'; // Common CSS class for widgets
  widgetDiv.id = title.toLowerCase(); // Unique ID based on title

  widgetDiv.innerHTML = `<h2>${widgetData.title}</h2>`;

  if (widgetData.content.kind === 'stack') {
    widgetData.content.content.forEach(item => {
      widgetDiv.appendChild(createElementFromItem(item));
    });
  } else if (widgetData.content.kind === 'text') {
    const textElement = document.createElement('p');
    textElement.textContent = widgetData.content.text;
    widgetDiv.appendChild(textElement);
  }
  // Add more conditions here for other 'content.kind' types if needed

  return widgetDiv;
}



function createElementFromItem(item) {
  let element;

  switch (item.kind) {
    case 'text':
      element = document.createElement('p');
      element.textContent = item.text;
      if (item.style) {
        applyTextStyles(element, item.style);
      }
      break;
    case 'button':
      element = document.createElement('button');
      element.id = item.id;
      element.textContent = item.text;
      // Event listener can be added if needed
      break;
    case 'table':
      element = createTable(item);
      break;
    case 'stack':
      element = document.createElement('div');
      item.content.forEach(nestedItem => {
        element.appendChild(createElementFromItem(nestedItem));
      });
      break;
      case 'image':
        element = document.createElement('img');
        // Use item.url instead of item.src
        if (item.url && (item.url.startsWith('data:image') || item.url.startsWith('http'))) {
          element.src = item.url;
        } else {
          console.error('Missing or invalid image data', item);
          element.alt = 'Missing image';
        }
        element.style.maxWidth = '100%';
      element.style.maxHeight = '400px'; // Set desired max height
        break;
    default:
      element = document.createElement('div');
      element.textContent = 'Unknown content type';
      break;
  }

  return element;
}

function applyTextStyles(element, style) {
  switch (style) {
    case 'bold':
      element.style.fontWeight = 'bold';
      break;
    case 'inactive':
      element.style.opacity = '0.5';
      element.style.fontStyle = 'italic';
      break;
    // Additional styles
  }
}

function createTable(tableData) {
  const table = document.createElement('table');
  const thead = document.createElement('thead');
  const tbody = document.createElement('tbody');

  if (tableData.header) {
    const headerRow = document.createElement('tr');
    tableData.header.forEach(headerItem => {
      const th = document.createElement('th');
      th.textContent = headerItem.text;
      headerRow.appendChild(th);
    });
    thead.appendChild(headerRow);
  }

  tableData.rows.forEach
    (rowData => {
      const row = document.createElement('tr');
      rowData.forEach(cellData => {
        const cell = cellData.kind === 'text' ? document.createElement('td') : createElementFromItem(cellData);
        if (cellData.kind === 'text') {
          cell.textContent = cellData.text;
        }
        row.appendChild(cell);
      });
      tbody.appendChild(row);
    });

  table.appendChild(thead);
  table.appendChild(tbody);
  return table;
}

function updateWidgets() {
  const apiURL = 'http://192.168.0.3:2030/widgets';

  fetch(apiURL)
    .then(response => {
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      return response.json();
    })
    .then(data => {
      const container = document.querySelector('.widget-container');
      container.innerHTML = ''; // Clear existing widgets

      // Process each widget in the 'widgets' object
      Object.values(data.widgets).forEach(widgetData => {
        const widgetElement = createWidget(widgetData);
        container.appendChild(widgetElement);
      });
    })
    .catch(error => {
      console.error(`Error loading or processing data from the API:`, error);
    });
}

document.addEventListener("DOMContentLoaded", function () {
  updateWidgets();
  setInterval(updateWidgets, 1000); // Refresh widgets every second
}); 