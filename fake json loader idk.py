import socket
import json

def send_json(socket, data):
    json_data = json.dumps(data).encode('utf-8')
    socket.sendall(json_data)
    socket.sendall(b'\0') # null-byte -> end of message

cargo_holder_widget = {
  "title": "Cargo Hold",
  "width": 4,
  "height": 4,
  "content": {
    "kind": "stack",
    "content": [
      {
        "kind": "table",
        "header": "",
        "rows": [
          [
            {
              "kind": "text",
              "text": "Credits"
            },
            {
              "kind": "text",
              "text": "42"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Used/Capacity"
            },
            {
              "kind": "text",
              "text": "7/140"
            }
          ]
        ]
      },
      {
        "kind": "table",
        "header": "",
        "rows": [
          [
            {
              "kind": "text",
              "text": "",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": "0",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": "1",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": "2",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": "3",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": "4",
              "style": "bold"
            }
          ],
          [
            {
              "kind": "text",
              "text": "0",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "1",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": "S"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "2",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": "S"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "3",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "4",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "5",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "6",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": "S"
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "7",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": "I"
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "8",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "9",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "10",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "11",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": "I"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "12",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "13",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "14",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "15",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "16",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "17",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "18",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "19",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "20",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "21",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "22",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "23",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "24",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "25",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "26",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ],
          [
            {
              "kind": "text",
              "text": "27",
              "style": "bold"
            },
            {
              "kind": "text",
              "text": "I"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": "I"
            },
            {
              "kind": "text",
              "text": " "
            },
            {
              "kind": "text",
              "text": " "
            }
          ]
        ]
      },
      {
        "kind": "table",
        "header": [
          {
            "kind": "text",
            "text": "Resource"
          },
          {
            "kind": "text",
            "text": "In hold"
          }
        ],
        "rows": [
          [
            {
              "kind": "text",
              "text": "Eisen"
            },
            {
              "kind": "text",
              "text": "4"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Stein"
            },
            {
              "kind": "text",
              "text": "3"
            }
          ]
        ]
      },
      {
        "kind": "table",
        "header": [
          {
            "kind": "text",
            "text": "Item"
          },
          {
            "kind": "text",
            "text": "Description"
          }
        ],
        "rows": [
          [
            {
              "kind": "text",
              "text": "Laderaumanbau"
            },
            {
              "kind": "text",
              "text": "Vergrössert den Laderaum um +20"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Thrust o Matic T33"
            },
            {
              "kind": "text",
              "text": "Mit diesem Wundergerät wird der Antrieb um 20% verbessert"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Scotty auf Steroiden"
            },
            {
              "kind": "text",
              "text": "Mit diesem Addon wird der Antrieb um 20% verbessert"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Scanner TX1000"
            },
            {
              "kind": "text",
              "text": "Scannt was das Zeugs hält"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Abbaulaser"
            },
            {
              "kind": "text",
              "text": "Laser, um Hindernisse aus dem Weg zu Räumen"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Boost o Matic X12"
            },
            {
              "kind": "text",
              "text": "Mit diesem Addon wird der Antrieb um 20% verbessert"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Permastore"
            },
            {
              "kind": "text",
              "text": "Modul, um Daten zu transferieren"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Eco Drive"
            },
            {
              "kind": "text",
              "text": "20% mehr Triebwerkleistung durch effizientere Energienutzung"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Long-Range-Communicator"
            },
            {
              "kind": "text",
              "text": "Verbindet weit entfernte Stationen miteinander"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Easy Jumper"
            },
            {
              "kind": "text",
              "text": "20% mehr Triebwerkleistung durch Nutzung des Unwahrscheinlichkeitsdrives"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Comm Module Shangris"
            },
            {
              "kind": "text",
              "text": "Implementation des Shangris Kommunikationsstandards"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Comm Module Elyse Terminal"
            },
            {
              "kind": "text",
              "text": "Implementation des Elyse Terminal Kommunikationsstandards"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Comm Module Artemis"
            },
            {
              "kind": "text",
              "text": "Implementation des Artemis Kommunikationsstandards"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Joker"
            },
            {
              "kind": "text",
              "text": "Macht das Quiz einfacher"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Joker"
            },
            {
              "kind": "text",
              "text": "Macht das Quiz einfacher"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Bessere Kristalle"
            },
            {
              "kind": "text",
              "text": "Durch bessere Kristalle beschleunigt sich der Abbau mit dem Laser um 20%"
            }
          ],
          [
            {
              "kind": "text",
              "text": "Laderaum X3000"
            },
            {
              "kind": "text",
              "text": "Durch Nutzung der Höhe vergrössert sich der Laderaum um +100"
            }
          ],
          [
            {
              "kind": "text",
              "text": "hackhack X1337"
            },
            {
              "kind": "text",
              "text": "Mysteriöses Gerät. Es verspricht Dinge, von denen alle Piraten nur träumen."
            }
          ]
        ]
      }
    ]
  }
}

communication_widget = {
  "title": "Communication",
  "width": 3,
  "height": 2,
  "content": {
    "kind": "stack",
    "content": [
      {
        "kind": "text",
        "text": "Elyse Terminal"
      },
      {
        "kind": "table",
        "header": [
          {
            "kind": "text",
            "text": "Resource"
          },
          {
            "kind": "text",
            "text": "Buy for"
          },
          {
            "kind": "text",
            "text": "Sell for"
          },
          {
            "kind": "text",
            "text": "In hold"
          },
          {
            "kind": "text",
            "text": ""
          },
          {
            "kind": "text",
            "text": ""
          }
        ],
        "rows": [
          [
            {
              "kind": "text",
              "text": "Platin"
            },
            {
              "kind": "text",
              "text": ""
            },
            {
              "kind": "text",
              "text": "999"
            },
            {
              "kind": "text",
              "text": "0"
            },
            {
              "kind": "text",
              "text": ""
            },
            {
              "kind": "button",
              "id": "{\"station\": \"Elyse Terminal\", \"what\": 2, \"action\": \"sell_resource\"}",
              "text": "Sell",
              "pressed": False
            }
          ],
          [
            {
              "kind": "text",
              "text": "Gold"
            },
            {
              "kind": "text",
              "text": "500"
            },
            {
              "kind": "text",
              "text": ""
            },
            {
              "kind": "text",
              "text": "0"
            },
            {
              "kind": "button",
              "id": "{\"station\": \"Elyse Terminal\", \"what\": 0, \"action\": \"buy_resource\"}",
              "text": "Buy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": ""
            }
          ]
        ]
      },
      {
        "kind": "table",
        "header": [
          {
            "kind": "text",
            "text": "Item"
          },
          {
            "kind": "text",
            "text": "Price"
          },
          {
            "kind": "text",
            "text": ""
          }
        ],
        "rows": [
          [
            {
              "kind": "stack",
              "content": [
                {
                  "kind": "text",
                  "style": "bold",
                  "text": "Joker"
                },
                {
                  "kind": "text",
                  "text": "Macht das Quiz einfacher"
                }
              ]
            },
            {
              "kind": "text",
              "text": "80000"
            },
            {
              "kind": "button",
              "id": "{\"station\": \"Elyse Terminal\", \"what\": \"joker_3\", \"action\": \"buy_item\"}",
              "text": "Buy",
              "pressed": False
            }
          ]
        ]
      },
      {
        "kind": "table",
        "header": "",
        "rows": []
      }
    ]
  }
}

easy_steering_widget = {
  "title": "Easy Steering",
  "width": 2,
  "height": 1,
  "content": {
    "kind": "stack",
    "content": [
      {
        "kind": "text",
        "text": "SpecialTarget.IDLE"
      },
      {
        "kind": "button",
        "id": "{\"action\": \"idle\"}",
        "text": "idle",
        "pressed": True
      },
      {
        "kind": "button",
        "id": "{\"action\": \"stop\"}",
        "text": "stop",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Core Station\"}",
        "text": "Core Station",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Vesta Station\"}",
        "text": "Vesta Station",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Azura Station\"}",
        "text": "Azura Station",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Shangris Station\"}",
        "text": "Shangris Station",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Platin Mountain\"}",
        "text": "Platin Mountain",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Artemis Station\"}",
        "text": "Artemis Station",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Elyse Terminal\"}",
        "text": "Elyse Terminal",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Illume Colony\"}",
        "text": "Illume Colony",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Zurro Station\"}",
        "text": "Zurro Station",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Phantom Station\"}",
        "text": "Phantom Station",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Tiberius Station\"}",
        "text": "Tiberius Station",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Nemesis Station\"}",
        "text": "Nemesis Station",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Research Station\"}",
        "text": "Research Station",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "{\"action\": \"goto\", \"name\": \"Architect Colony\"}",
        "text": "Architect Colony",
        "pressed": False
      }
    ]
  }
}

example_widget = {
    "title": "Example widget",
    "width": 1,
    "height": 1,
    "content": {
      "kind": "text",
      "text": "This is an example widget. Please see the doc"
    }
}

laser_widget = {
  "title": "Laser",
  "width": 2,
  "height": 1,
  "content": {
    "kind": "stack",
    "content": [
      {
        "kind": "text",
        "text": "2023-12-05T14:37:12.067024"
      },
       {
        "kind": "text",
        "text": "Aktueller Status: Freigeschaltet und inaktiv"
      }
    ]
  }
}

long_range_com_widget = {
  "title": "Long Range Communicator",
  "width": 3,
  "height": 1,
  "content": {
    "kind": "stack",
    "content": [
      {
        "kind": "text",
        "text": "2024-01-16T08:22:58.960813"
      },
      {
        "kind": "table",
        "header": [
          {
            "kind": "text",
            "text": "System"
          },
          {
            "kind": "text",
            "text": "Status"
          }
        ],
        "rows": [
          [
            {
              "kind": "text",
              "text": "coupler"
            },
            {
              "kind": "text",
              "text": "transmitting: 0 messages transmitted, errors=1) (go to log for details)"
            }
          ],
          [
            {
              "kind": "text",
              "text": "db"
            },
            {
              "kind": "text",
              "text": "connected"
            }
          ],
          [
            {
              "kind": "text",
              "text": "scanner"
            },
            {
              "kind": "text",
              "text": "received positions"
            }
          ]
        ]
      }
    ]
  }
}

missions_widget = {
    "title": "Mission Computer",
    "width": 3,
    "height": 4,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "stack",
          "content": [
            {
              "kind": "text",
              "style": "bold",
              "text": "Dringende Lieferung"
            },
            {
              "kind": "text",
              "text": "Wir brauchen noch 60 Gold in Core Station!"
            },
            {
              "kind": "text",
              "style": "inactive",
              "text": "Wir brauchen dringend Gold, um die neuste iWatch 133 herstellen zu können. Ein Aufstand der Stationsbewohner muss abgewendet werden"
            },
            {
              "kind": "text",
              "text": ""
            }
          ]
        },
        {
          "kind": "stack",
          "content": [
            {
              "kind": "text",
              "style": "bold",
              "text": "Forschungsdaten von Elyse Terminal"
            },
            {
              "kind": "text",
              "text": "Wunderbar. Jetzt müssen die Daten nur noch heruntergeladen werden und dann bei Artemis Station hochgeladen werden."
            },
            {
              "kind": "text",
              "style": "inactive",
              "text": "Es sind wichtige Forschungsdaten verfügbar, welche nach Artemis Station gebracht werden sollen. Bitte hole die Daten von Elyse Terminal ab. Die Daten können mit dem Permastore Modul heruntergeladen werden."
            },
            {
              "kind": "text",
              "text": ""
            }
          ]
        },
        {
          "kind": "stack",
          "content": [
            {
              "kind": "text",
              "style": "bold",
              "text": "Signalbrücke"
            },
            {
              "kind": "text",
              "text": "Die Artemis Station möchte gerne der Shangris Station einige Daten austauschen. Damit die Artemis Station mit der Shangris Station kommunizieren kann, muss der Long-Range-Communicator in Betrieb genommen werden."
            },
            {
              "kind": "text",
              "text": ""
            }
          ]
        },
        {
          "kind": "text",
          "style": "bold",
          "text": "Abgeschlossene Missionen"
        },
        {
          "kind": "text",
          "text": "Abgeschlossene Missionen: Statue Iron Fist, Eingefrorene Station, Shangris Station, Partnerstation Artemis, Platin"
        }
      ]
    }
  }

navigation_widget = {
  "title": "Navigation",
  "width": 2,
  "height": 1,
  "content": {
    "kind": "text",
    "text": "19942.6/59527.5 62.0°"
  }
}

installer_widget = {
  "title": "Installer",
  "width": 4,
  "height": 4,
  "content": {
    "kind": "stack",
    "content": [
      {
        "kind": "table",
        "header": [
          {
            "kind": "text",
            "text": "Component"
          },
          {
            "kind": "text",
            "text": ""
          },
          {
            "kind": "text",
            "text": ""
          },
          {
            "kind": "text",
            "text": "Expected/Ready"
          },
          {
            "kind": "text",
            "text": "Ports"
          }
        ],
        "rows": [
          [
            {
              "kind": "text",
              "text": "cockpit_frontend"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"cockpit_frontend\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": ""
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": "2000"
            }
          ],
          [
            {
              "kind": "text",
              "text": "cockpit_backend"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"cockpit_backend\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": ""
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": "2001, 2002"
            }
          ],
          [
            {
              "kind": "text",
              "text": "thruster"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"thruster\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"thruster\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "6/6"
            },
            {
              "kind": "text",
              "text": "2003, 2004, 2005, 2006, 2007, 2008"
            }
          ],
          [
            {
              "kind": "text",
              "text": "navigation"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"navigation\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"navigation\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": "2010"
            }
          ],
          [
            {
              "kind": "text",
              "text": "easy_steering"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"easy_steering\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"easy_steering\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "0/0"
            },
            {
              "kind": "text",
              "text": ""
            }
          ],
          [
            {
              "kind": "text",
              "text": "communication"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"communication\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"communication\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": "2011"
            }
          ],
          [
            {
              "kind": "text",
              "text": "cargo_hold"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"cargo_hold\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"cargo_hold\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": "2012"
            }
          ],
          [
            {
              "kind": "text",
              "text": "mission_computer"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"mission_computer\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"mission_computer\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": "2022"
            }
          ],
          [
            {
              "kind": "text",
              "text": "installer"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"installer\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": ""
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": ""
            }
          ],
          [
            {
              "kind": "text",
              "text": "cockpit_example"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"cockpit_example\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"cockpit_example\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": ""
            }
          ],
          [
            {
              "kind": "text",
              "text": "scanner"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"scanner\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"scanner\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": ""
            }
          ],
          [
            {
              "kind": "text",
              "text": "laser"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"laser\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"laser\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": "2018"
            }
          ],
          [
            {
              "kind": "text",
              "text": "permastore"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"permastore\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"permastore\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": "2019"
            }
          ],
          [
            {
              "kind": "text",
              "text": "longrangecommunicator"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"longrangecommunicator\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"longrangecommunicator\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": ""
            }
          ],
          [
            {
              "kind": "text",
              "text": "comm-module-artemis"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"comm-module-artemis\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"comm-module-artemis\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": "2024"
            }
          ],
          [
            {
              "kind": "text",
              "text": "comm-module-shangris"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"comm-module-shangris\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"comm-module-shangris\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": "2025"
            }
          ],
          [
            {
              "kind": "text",
              "text": "comm-module-elyse"
            },
            {
              "kind": "button",
              "id": "{\"action\": \"deploy\", \"what\": \"comm-module-elyse\"}",
              "text": "deploy",
              "pressed": False
            },
            {
              "kind": "button",
              "id": "{\"action\": \"undeploy\", \"what\": \"comm-module-elyse\"}",
              "text": "undeploy",
              "pressed": False
            },
            {
              "kind": "text",
              "text": "1/1"
            },
            {
              "kind": "text",
              "text": "2026"
            }
          ]
        ]
      }
    ]
  }
}

scanner_widget = {
  "title": "Scanner",
  "width": 2,
  "height": 1,
  "content": {
    "kind": "stack",
    "content": [
      {
        "kind": "text",
        "text": "Status: connected"
      },
      {
        "kind": "table",
        "header": [
          {
            "kind": "text",
            "text": "name"
          },
          {
            "kind": "text",
            "text": "pos"
          }
        ],
        "rows": [
          [
            {
              "kind": "text",
              "text": "Elyse Terminal"
            },
            {
              "kind": "text",
              "text": "-9000.0/-7500.0"
            }
          ]
        ]
      }
    ]
  }
}

sort_bot_widget = {
  "title": "Sort bot for Sell",
  "width": 2,
  "height": 2,
  "doc": "\n# Example use\n```shell\n$ curl -XPUT http://192.168.0.3:2004/thruster --data '{\"thrust_percent\": 0}'\n$ curl -XPUT http://192.168.0.3:2004/thruster --data '{\"thrust_percent\": 50}'\n$ curl -XPUT http://192.168.0.3:2004/thruster --data '{\"thrust_percent\": 100}'\n```\n",
  "content": {
    "kind": "stack",
    "content": [
      {
        "kind": "text",
        "text": "The selected material will move to the front of the Laggerraum"
      },
      {
        "kind": "button",
        "id": "IRON",
        "text": "IRON",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "STONE",
        "text": "STONE",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "GOLD",
        "text": "GOLD",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "PLATIN",
        "text": "PLATIN",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "URAN",
        "text": "URAN",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "IRIDIUM",
        "text": "IRIDIUM",
        "pressed": False
      },
      {
        "kind": "button",
        "id": "DOWN",
        "text": "ALL DOWN",
        "pressed": False
      }
    ]
  }
}

thruster_front_widget = {
     "title": "Thruster front",
    "width": 1,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "25.0%"
        },
        {
          "kind": "button",
          "id": "boost",
          "text": "Boost",
          "pressed": False
        }
      ]
    }
}

thruster_front_right_widget = {
    "title": "Thruster frontRight",
    "width": 1,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "100.0%"
        },
        {
          "kind": "button",
          "id": "boost",
          "text": "Boost",
          "pressed": False
        }
      ]
    }
}

thruster_front_left_widget = {
    "title": "Thruster frontLeft",
    "width": 1,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "0.0%"
        },
        {
          "kind": "button",
          "id": "boost",
          "text": "Boost",
          "pressed": False
        }
      ]
    }
}

thruster_bottom_widget = {
    "title": "Thruster back",
    "width": 1,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "100.0%"
        },
        {
          "kind": "button",
          "id": "boost",
          "text": "Boost",
          "pressed": False
        }
      ]
    }
}

thruster_bottom_right_widget = {
    "title": "Thruster bottomRight",
    "width": 1,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "0.0%"
        },
        {
          "kind": "button",
          "id": "boost",
          "text": "Boost",
          "pressed": False
        }
      ]
    }
}

thruster_bottom_left_widget = {
    "title": "Thruster bottomLeft",
    "width": 1,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "0.0%"
        },
        {
          "kind": "button",
          "id": "boost",
          "text": "Boost",
          "pressed": False
        }
      ]
    }
}

hacking_device = {
    "title": "Hacking Device",
    "width": 2,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "Status: ready"
        }
      ]
    }
}

comm_module_core = {
    "title": "Comm Module Core",
    "width": 2,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "2024-01-16T07:46:15.128092"
        },
        {
          "kind": "text",
          "text": "ready"
        }
      ]
    }
}

comm_module_shangris = {
    "title": "Comm Module Core",
    "width": 2,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "2024-01-16T07:46:15.125825"
        },
        {
          "kind": "text",
          "text": "ready"
        }
      ]
    }
}

comm_module_artemis = {
    "title": "Comm Module Artemis",
    "width": 2,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "2024-01-16T08:27:36.067147"
        },
        {
          "kind": "text",
          "text": "ready"
        }
      ]
    }
}

comm_module_zurro = {
  "title": "Comm Module Zurro",
    "width": 2,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "2024-01-16T07:46:15.126168"
        },
        {
          "kind": "text",
          "text": "ready"
        }
      ]
    }
}

comm_module_elyse = {
  "title": "Comm Module Elyse",
    "width": 2,
    "height": 1,
    "content": {
      "kind": "stack",
      "content": [
        {
          "kind": "text",
          "text": "2024-01-16T07:46:15.130627"
        },
        {
          "kind": "text",
          "text": "ready"
        }
      ]
    }
}

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect(('127.0.0.1', 2002))

    # Sends widget
    send_json(s, cargo_holder_widget)
    send_json(s, communication_widget)
    send_json(s, easy_steering_widget)
    send_json(s, scanner_widget)
    send_json(s, example_widget)
    send_json(s, laser_widget)
    send_json(s, long_range_com_widget)
    send_json(s, missions_widget)
    send_json(s, navigation_widget)
    send_json(s, installer_widget)
    send_json(s, sort_bot_widget)
    send_json(s, thruster_front_widget)
    send_json(s, thruster_front_right_widget)
    send_json(s, thruster_front_left_widget)
    send_json(s, thruster_bottom_widget)
    send_json(s, thruster_bottom_right_widget)
    send_json(s, thruster_bottom_left_widget)
    send_json(s, hacking_device)
    send_json(s, comm_module_core)
    send_json(s, comm_module_shangris)
    send_json(s, comm_module_artemis)
    send_json(s, comm_module_zurro)
    send_json(s, comm_module_elyse)

    # Listen for any response
    buffer = bytes()
    while True:
        received = s.recv(1)
        if len(received) == 0:
            print('Disconnected')
            break
        if received == b'\0':
            print('Received:', buffer)
            send_json(s, {'kind': 'keepalive'}) # keepalive message
            buffer = bytes()
        else:
            buffer += received
